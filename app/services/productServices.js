const  BASE_URL = "https://6487251dbeba62972790173a.mockapi.io/QLSV";
var productServices = {
    getList: () => {
        return axios({
            url: BASE_URL,
            method: "GET",
        });
    },
    delete: (id) => {
        return axios({
            url: `${BASE_URL}/${id}`,
            method: "DELETE",
        });
    },
    create: (QLSV) => {
        return axios({
            url: BASE_URL,
            method: "POST",
            data: QLSV,
        });
    },
    getById: (id) => {
        return axios({
            url: `${BASE_URL}/${id}`,
            method: "GET",
             
        });
    },
    update: (id, QLSV) => {
        return axios({
            url: `${BASE_URL}/${id}`,
            method: "PUT",
            data: QLSV,
        }) ;
    },

}