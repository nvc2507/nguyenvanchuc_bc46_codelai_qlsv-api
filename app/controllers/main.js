
var idQLSVUpdate = null;

// Lấy danh sách sinh viên từ sever và render ra layout
function fetchQLSVList(){
    onLoading();
    productServices.getList()
    .then(function(res){
        renderDSSV(res.data);
        offLoading();
    })
    .catch(function(err){
        offLoading();
    });
}
fetchQLSVList();







// reset form in put
document.getElementById("formQLSV").reset();

// thêm SV
function themSinhVien(){
    var newSV = layThongTinTuForm();
    productServices.create(newSV)
    .then(function(res){
        fetchQLSVList();
    })
    .catch(function(err){

    });
}

// Xóa sinh viên 

function xoaSinhVien(id){
    onLoading();
    productServices.delete(id)
    .then(function(res){
        fetchQLSVList();
        offLoading();
    })
    .catch(function(err){
        offLoading();
    })
};

// sửa sinh viên

function suaSinhVien(id){
    idQLSVUpdate = id;
    $('#myModal').modal('show');
    productServices.getById(id)
    .then(function(res){
        offLoading();
        showThongTinLenFrom(res.data);
    })
    .catch(function(err){
        offLoading();
    })
};

// Cập nhật sinh viên
function capNhatSinhVien() {
    // Lấy thông tin từ form
    var sv = layThongTinTuForm();
    productServices.update(idQLSVUpdate, sv)
    .then(function(res){
        fetchQLSVList();

    })
    .catch(function(err){

    });
    
   
   
    document.getElementById("formQLSV").reset();
}

