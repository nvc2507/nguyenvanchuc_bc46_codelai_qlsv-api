//render DSSV lên table

function renderDSSV(dssv){
    var contentHTML = "";
    
    dssv.forEach(function(item){
        var content = `
        <tr>
        <td>${item.id}</td>
        <td>${item.name}</td>
        <td>${item.email}</td>
        <td>${item.diemTB}</td>
        <td>
        <button onclick="xoaSinhVien(${item.id})"
        class="btn btn-danger">Xóa</button>
        <button onclick="suaSinhVien(${item.id})"
        class="btn btn-warning">Sửa</button>
        </td>
        </tr>
        `;
        contentHTML += content;
    });
    document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function onLoading(){
    document.getElementById("spinner").style.display = "flex";
}
function offLoading(){
    document.getElementById("spinner").style.display = "none";
}


// lấy dữ liệu từ form input
function layThongTinTuForm(){
    var id = document.getElementById("txtMaSV").value;
    var name = document.getElementById("txtTenSV").value;
    var email = document.getElementById("txtEmail").value;
    var password = document.getElementById("txtPass").value;
    var toan = document.getElementById("txtDiemToan").value*1;
    var ly = document.getElementById("txtDiemLy").value*1;
    var hoa = document.getElementById("txtDiemHoa").value*1;


    return {
        id: id,
        name: name,
        email: email,
        password: password,
        toan: toan,
        ly: ly,
        hoa: hoa,
        
    };

}

// show thông tin lên form khi sửa SV

function showThongTinLenFrom(QLSV){
    document.getElementById("txtMaSV").value = QLSV.id;
    document.getElementById("txtTenSV").value = QLSV.name;
    document.getElementById("txtEmail").value = QLSV.email;
    document.getElementById("txtPass").value = QLSV.password;
    document.getElementById("txtDiemToan").value = QLSV.toan;
    document.getElementById("txtDiemLy").value = QLSV.ly;
    document.getElementById("txtDiemHoa").value = QLSV.hoa;
}